# TMENU - Terminal Menu

Tmenu is a program which allows defining interactive, visually pleasant
terminal, multi-level menus by describing them in TOML configuration files.

Each menu item is described as a separate TOML table. Depending on its
contents, it is interpreted as a sub-menu or leaf node. Sub-menus contain a
list of other sub-menus and leaf nodes, while leaf nodes, when selected,
execute defined command inside the spawned sub-shell.

Main menu, which is displayed when program starts, must be always defined as
a `[root]` table.

Sub-menus are identified by table names, not `name` value inside the table

See `example.toml` for example menu configuration file.

## Navigating menus

- each item can be accessed by pressing a key associated with a `key` field
  defined inside item's table
- _escape_ goes back to the previous submenu, or quits, if tmenu displays a
  root menu
- _ctrl + c_ always quits tmenu

## Special items

- `_quit` - special item activated with a capital `Q` shortcut, which quits
  the program at any time
- `_sep` - empty line used to visually separate menu items

## License

Copyright 2021 Michał Góral.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
