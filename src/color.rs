/* SPDX-License-Identifier: GPL-3.0-or-later */

use std::str::FromStr;
use std::collections::HashMap;

use termion::color::*;

#[derive(Debug, PartialEq)]
struct RGB {
    r: u8,
    g: u8,
    b: u8,
}

impl FromStr for RGB {
    type Err = std::num::ParseIntError;

    fn from_str(hex_code: &str) -> Result<Self, Self::Err> {
        let r: u8 = u8::from_str_radix(&hex_code[1..3], 16)?;
        let g: u8 = u8::from_str_radix(&hex_code[3..5], 16)?;
        let b: u8 = u8::from_str_radix(&hex_code[5..7], 16)?;

        Ok(RGB { r, g, b })
    }
}

fn named_colors() -> HashMap<String, Box<dyn Color>> {
    let mut hm : HashMap<_, Box<dyn Color>> = HashMap::new();

    hm.insert("blue".to_string(), Box::new(termion::color::Blue));
    hm.insert("bright green".to_string(), Box::new(termion::color::LightGreen));
    hm.insert("black".to_string(), Box::new(termion::color::Black));
    hm.insert("cyan".to_string(), Box::new(termion::color::Cyan));
    hm.insert("green".to_string(), Box::new(termion::color::Green));
    hm.insert("bright black".to_string(), Box::new(termion::color::LightBlack));
    hm.insert("bright blue".to_string(), Box::new(termion::color::LightBlue));
    hm.insert("bright cyan".to_string(), Box::new(termion::color::LightCyan));
    hm.insert("bright magenta".to_string(), Box::new(termion::color::LightMagenta));
    hm.insert("bright red".to_string(), Box::new(termion::color::LightRed));
    hm.insert("bright white".to_string(), Box::new(termion::color::LightWhite));
    hm.insert("bright yellow".to_string(), Box::new(termion::color::LightYellow));
    hm.insert("magenta".to_string(), Box::new(termion::color::Magenta));
    hm.insert("red".to_string(), Box::new(termion::color::Red));
    hm.insert("white".to_string(), Box::new(termion::color::White));
    hm.insert("yellow".to_string(), Box::new(termion::color::Yellow));

    hm
}

pub fn parse(s: &String) -> Option<String> {
    if s == "" {
        return None
    }

    let sl = s.to_lowercase();

    if sl.starts_with("#") {
        let c = RGB::from_str(&sl).ok()?;
        return Some(format!("{}", termion::color::Fg(termion::color::Rgb(c.r, c.g, c.b))));
    }

    if let Some(colno) = sl.parse::<u8>().ok() {
        return Some(format!("{}", termion::color::Fg(termion::color::AnsiValue(colno))));
    }

    if let Some(colno) = named_colors().get(&sl) {
        return Some(format!("{}", termion::color::Fg(colno.as_ref())));
    }

    None
}
