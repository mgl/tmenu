/* SPDX-License-Identifier: GPL-3.0-or-later */

extern crate serde;
extern crate structopt;
extern crate toml;
extern crate termion;

use std::io;
use std::io::prelude::*;
use std::path::PathBuf;
use std::fs::File;
use std::process::{exit, Command};
use std::{thread, time};

use termion::event::Key;
use termion::input::TermRead;

use structopt::StructOpt;

mod term;
use term::{Screen, Input};

mod cfg;
mod msg;
mod color;

#[derive(StructOpt, Debug)]
#[structopt(
    name = "tmenu",
    verbatim_doc_comment
)]
/// tmenu - create interactive shell menus from toml description.
struct Opt {
    #[structopt(name = "CONFIG")]
    /// Path to TOML file with menu description
    config: PathBuf,
}

fn parse_config(path : &PathBuf) -> Result<cfg::Config, io::Error> {
    let mut file = File::open(path)?;
    let mut s = String::new();
    file.read_to_string(&mut s)?;

    match toml::from_str(&s) {
        Ok(cfg) => return Ok(cfg),
        Err(e) => {
            eprintln!("{}: {:}", path.to_str().unwrap_or("config"),  e);
            return Err(io::Error::new(
                io::ErrorKind::InvalidData, "parsing error"));
        },
    };
}

fn print_title(screen: &mut impl term::Screen, item: &cfg::Item) {
    let mut shfmt = String::new();
    if let Some(key) = item.key {
        let cdef = item.color.as_ref().map_or_else(String::new, |v| v.clone());
        shfmt = format!("{}{}[{}]{}{} ",
            termion::style::Bold,
            color::parse(&cdef).unwrap_or_else(String::new),
            key,
            termion::style::Reset,
            termion::color::Fg(termion::color::Reset));
    }

    let fmt = format!("{}{}", shfmt, item.name);
    screen.print(&fmt);
}

fn print_menu(screen: &mut impl term::Screen, items: &Vec<String>, config: &cfg::Config) {
    for subitem in items {
        if let Some(subtable) = config.tables.get(subitem) {
            print_title(screen, &subtable);
        }
    }
}

fn print_item(screen: &mut impl term::Screen, item: &cfg::Item, config: &cfg::Config) {
    let u = "-".repeat(item.name.chars().count());
    screen.print(&item.name);
    screen.print(u);
    screen.print("");

    match &item.items {
        Some(items) => {
            print_menu(screen, &items, &config);
        }
        None => {
            print_title(screen, &item);
        }
    }
}

fn get_message<'a>(name: &String, t: &'a cfg::Item) -> Option<msg::Message<'a>> {
    if t.items.is_some() {
        return Some(msg::Message::Push(msg::Push{table: name.to_owned()}));
    }
    else if t.exec.is_some() {
        return Some(msg::Message::Exec(msg::Exec{table: &t}));
    }
    else if *name == "_quit".to_string() {
        return Some(msg::Message::Quit);
    }
    None
}

fn input<'a>(key: char, stack: &mut Vec<String>, config: &'a cfg::Config) -> Option<msg::Message<'a>> {
    let empty = Vec::new();
    let items = config.tables.get(stack.last().unwrap())
        .map(|t| t.items.as_ref().unwrap_or(&empty))
        .unwrap_or(&empty);

    for name in items {
        match config.tables.get(name) {
            None => (),
            Some(t) => match t.key {
                None => (),
                Some(sh) => {
                    if sh != key { continue; }
                    return get_message(&name, &t);
                }
            }
        }
    }

    None
}

fn run_command(cmd: &String, waitafter: u64, waitforkeypress: bool, term: &mut impl Screen) {
    term.suspend_raw();
    let status = Command::new("sh").arg("-c").arg(&cmd).status();
    term.raw();

    if status.is_err() {
        term.print("Failed to execute a shell...");
        term.render();
        std::io::stdin().keys().next();
        return;
    }

    if waitafter > 0 {
        thread::sleep(time::Duration::from_secs(waitafter));
    }

    if waitforkeypress {
        term.print("Process terminated, press any key...");
        term.render();
        std::io::stdin().keys().next();
    }
}

fn run(config: &cfg::Config) {
    let mut term = term::Terminal::new(std::io::stdin(), std::io::stdout(), &config);

    let mut stack = Vec::new();
    stack.push("root".to_string());

    loop {
        match stack.last() {
            Some(name) => {
                if let Some(table) = config.tables.get(name) {
                    term.clear();
                    print_item(&mut term, &table, &config);
                    term.render();
                }
                else {
                    break;
                }
            }
            None => break,
        }

        match term.key() {
            Key::Esc => { stack.pop(); }
            Key::Ctrl('c') => { break; }
            Key::Char(c) => { 
                if let Some(m) = input(c, &mut stack, &config) {
                    match m {
                        msg::Message::Quit => break,
                        msg::Message::Push(p) => { stack.push(p.table); }
                        msg::Message::Exec(e) => {
                            let cmd = e.table.exec.as_ref().unwrap();
                            let waittime = e.table.wait_after.unwrap_or(0);
                            let waitkey = e.table.wait_for_keypress.unwrap_or(false);
                            run_command(&cmd, waittime, waitkey, &mut term);

                            if config.stop_after_exec || e.table.stop_after_exec.unwrap_or(false) {
                                break;
                            }
                        }
                    }
                }; 
            }
            _ => {}
        }
    }
}

fn fix_names(cfg: &mut cfg::Config) {
    for (key, val) in cfg.tables.iter_mut() {
        if val.name.is_empty() {
            val.name = key.to_owned();
        }
    }
}

fn add_special_tables(cfg: &mut cfg::Config) {
    cfg.tables.insert("_sep".to_string(), cfg::Item::new());
    cfg.tables.insert("_quit".to_string(), cfg::Item::new_quit());
}

fn main() -> Result<(), io::Error> {
    let opt = Opt::from_args();
    let mut cfg = parse_config(&opt.config)?;

    fix_names(&mut cfg);
    add_special_tables(&mut cfg);

    match cfg.tables.get("root") {
        None => {
            eprintln!("Missing [root] table in configuration");
            exit(1);
        }
        Some(_) => {
            run(&cfg);
        }
    }

    Ok(())
}
