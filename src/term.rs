/* SPDX-License-Identifier: GPL-3.0-or-later */

extern crate termion;

use termion::event::Key;
use termion::input::TermRead;
use termion::raw::{IntoRawMode, RawTerminal};
use termion::cursor::DetectCursorPos;

use cfg;

pub trait Screen {
    fn print<S: AsRef<str>>(&mut self, s: S);
    fn clear(&mut self);
    fn render(&mut self);
    fn raw(&mut self);
    fn suspend_raw(&mut self);
}

pub trait Input {
    fn key(&mut self) -> Key;
}

pub struct Terminal<R, W: std::io::Write> {
    stdout: W,
    input: R,

    leftpad: u16,

    lineno: u16,
}

impl<R: std::io::Read, W: std::io::Write> Terminal<R, W> {
    pub fn new(stdin: R, stdout: W, cfg: &cfg::Config) -> Terminal<termion::input::Keys<R>, RawTerminal<W>> {
        Terminal {
            stdout: stdout.into_raw_mode().unwrap(),
            input: stdin.keys(),
            leftpad: 1 + cfg.padding_left,

            lineno: 1,
        }
    }
}

impl<R, W: std::io::Write> Screen for Terminal<R, RawTerminal<W>> {
    fn print<S: AsRef<str>>(&mut self, s: S) {
        write!(self.stdout, "{}{}{}\r\n",
            termion::cursor::Goto(self.leftpad, self.lineno),
            s.as_ref(),
            termion::cursor::Hide).unwrap();
        self.lineno += 1;
    }

    fn clear(&mut self) {
        write!(self.stdout, "{}{}", 
            termion::clear::All,
            termion::cursor::Goto(1, 1)).unwrap();
        self.lineno = 1;
    }

    fn render(&mut self) {
        self.stdout.flush().unwrap();
    }

    fn suspend_raw(&mut self) {
        write!(self.stdout, "{}", termion::cursor::Show).unwrap();
        self.stdout.flush().unwrap();
        self.stdout.suspend_raw_mode().expect("Failed to suspend raw mode");
    }

    fn raw(&mut self) {
        self.stdout.activate_raw_mode().expect("Failed to enter raw mode");

        let (_, y) = self.stdout.cursor_pos().unwrap_or((0, 0));
        if self.lineno !=  y {
            self.lineno = y + 1;
        }
    }
}

impl<R: Iterator<Item=Result<Key, std::io::Error>>, W: std::io::Write> Input for Terminal<R, W> {
    fn key(&mut self) -> Key {
        self.input.next().unwrap().unwrap()
    }
}

impl<R, W: std::io::Write> Drop for Terminal<R, W> {
    fn drop(&mut self) {
        write!(self.stdout, "{}", termion::cursor::Show).unwrap();
        self.stdout.flush().unwrap();
    }
}
