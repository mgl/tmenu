/* SPDX-License-Identifier: GPL-3.0-or-later */

extern crate serde;

use std::collections::HashMap;
use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct Config {
    #[serde(default)]
    pub padding_left : u16,

    #[serde(default)]
    pub stop_after_exec : bool,

    #[serde(flatten)]
    pub tables: HashMap<String, Item>,
}

#[derive(Deserialize, Debug)]
pub struct Item {
    #[serde(default)]
    pub name: String,
    pub key: Option<char>,
    pub color: Option<String>,

    pub items: Option<Vec<String>>,
    pub exec: Option<String>,
    pub wait_for_keypress: Option<bool>,
    pub wait_after: Option<u64>,
    pub stop_after_exec: Option<bool>,
}

impl Item {
    pub fn new() -> Item {
        Item {
            name: "".to_string(),
            key: None,
            color: None,
            items: None,
            exec: None,
            wait_for_keypress: None,
            wait_after: None,
            stop_after_exec: None,
        }
    }

    pub fn new_quit() -> Item {
        let mut item = Item::new();
        item.name = "Quit".to_string();
        item.key = Some('Q');
        item.color = Some("red".to_string());

        item
    }
}
