/* SPDX-License-Identifier: GPL-3.0-or-later */

use cfg::Item;

pub struct Push {
    pub table: String
}

pub struct Exec<'a> {
    pub table: &'a Item
}

pub enum Message<'a> {
    Quit,
    Push(Push),
    Exec(Exec<'a>),
}
