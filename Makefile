include config.mk

all: build

build:
	cargo build $(CARGOFLAGS)

install: build
	cargo install --offline --path . --root $(DESTDIR)$(PREFIX)

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/tmenu

clean:
	rm -rf target

.PHONY: all build install uninstall clean
